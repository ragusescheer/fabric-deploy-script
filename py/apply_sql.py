from modules import engine
from modules import run
from modules import out
from modules import mysql
from modules import transfer


@engine.prepare_and_clean
def execute(mysql_file, location = None):
    if location is None:
        location = 'local'
    if not (location == 'local' or location == 'remote'):
        out.log('unknown location: ' + str(location), 'command', out.LEVEL_ERROR)
        engine.quit()
    if not engine.local_is_not_empty(mysql_file):
        out.log('file not found: ' + str(mysql_file), 'command', out.LEVEL_ERROR)
        engine.quit()

    out.log("executing mysql file " + mysql_file + " to " + location + " database.")
    
    if location == 'remote':
        remote_file = transfer.put(mysql_file)
        mysql.execute_remote_file(remote_file)
    else:
        mysql.execute_local_file(mysql_file)




def help():
    out.log("This command applies an sql file to the a database. The first argument must be a sql file, the second argument is optional and specifies the location: either local (default) or remote.", 'help')
