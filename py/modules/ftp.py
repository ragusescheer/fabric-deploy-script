import engine
import run
import out
import buffering


#writes the commands to file and executes the file. This way we circumvent all the escaping trouble.
@buffering.buffered
@out.indent
def execute(command):
    out.log(command, 'ftp', out.LEVEL_VERBOSE)

    #assemble login data
    login = "quote USER " + str(engine.FTP_USER) + "\nquote PASS " + str(engine.FTP_PASSWORD) + "\n"
    if hasattr(engine, 'FTP_QUOTE_DISABLE'):
        if engine.FTP_QUOTE_DISABLE:
            login = ""

    #write the command into file
    ftp_file = engine.write_local_file(login + command, 'ftp')

    #run the ftp file
    if engine.FTP_FORCE_IP4:
        ip4_arg = '-4 '
    else:
        ip4_arg = ''
    if engine.FTP_CLIENT == 'ftp':
        ftp_command_line = 'ftp -p -n ' + ip4_arg + engine.FTP_HOST + ' <' + ftp_file
    elif engine.FTP_CLIENT == 'sftp':
        ftp_command_line = 'sftp -b ' + ip4_arg + ftp_file + ' ' + engine.escape(engine.FTP_USER) + '@' + engine.FTP_HOST
    else:
        out.error('unknown FTP_CLIENT: ' + str(engine.FTP_CLIENT))
    run.local(ftp_command_line, retry = 3)


#setup and expose buffering interface
execute.set_name('ftp')
def start_buffer():
    execute.start_buffer()
def flush_buffer():
    execute.flush_buffer()
def end_buffer():
    execute.end_buffer();
def has_buffer():
    return execute.has_buffer()
